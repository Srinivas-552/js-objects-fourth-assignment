
function keys(obj) {
    const keysAsStringArr = [];
    for (let key in obj) {
        keysAsStringArr.push(key);
    }
    return keysAsStringArr;
}

module.exports = { keys };