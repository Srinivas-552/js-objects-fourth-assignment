
function values(obj) {
    const objValuesArr = [];
    if (!obj) {
        return {};
    }
    for (let key in obj) {
        const value = obj[key];
        objValuesArr.push(value);
    }
    return objValuesArr;
}

module.exports = { values };