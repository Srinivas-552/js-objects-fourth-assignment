
const defaultPropsModule = require('../defaults');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };    

// const testObject = { name: undefined, age: 36, location: 'Gotham' };     //check `undefined` value   

const defaultProps = {name: 'Srinivas', age:26, location:'Hyderabad', company:'Mountblue'}

const result = defaultPropsModule.defaults(testObject, defaultProps)

console.log(result);