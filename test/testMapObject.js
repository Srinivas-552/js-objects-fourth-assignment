
const mapObjectModule = require('../mapObject');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function cb(value, key, obj){
    return `Transformed value is ${value}`;
}

const result = mapObjectModule.mapObject(testObject, cb)

console.log(result);