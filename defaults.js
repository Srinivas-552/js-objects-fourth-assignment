
function defaults(obj, defaultProps) {
    const resultObj = obj;

    // const res = Object.assign({}, defaultProps, obj)
    // console.log(res)
    if (!obj && !defaultProps) {
        return {};
    }
    for (let key in defaultProps) {
        const value = defaultProps[key];

        if (!(key in obj) || (obj[key] === undefined)){
            resultObj[key] = value
        }
    }
    return resultObj;
}

module.exports = { defaults };