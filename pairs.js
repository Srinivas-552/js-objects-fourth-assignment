
function pairs(obj) {
    const keyValueArr = [];
    if (!obj) {
        return {};
    }
    for (let key in obj) {
        const value = obj[key];
        const keyValuePair = [key, value];
        keyValueArr.push(keyValuePair);
    }
    return keyValueArr;
}

module.exports = { pairs };