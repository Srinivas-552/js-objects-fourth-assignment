
function mapObject(obj, myCallBackFn) {
    const resultObj = {};
    if (!obj) {
        return {};
    }
    for (let key in obj) {
        const value = obj[key];
        const transformedValue = myCallBackFn(value, key, obj);
        resultObj[key] = transformedValue;
    }
    return resultObj;
}

module.exports = { mapObject };