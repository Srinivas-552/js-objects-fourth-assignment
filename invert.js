
function invert(obj) {
    const invertedKeyValueObj = {};
    // const strObj = JSON.stringify(obj);

    if (!obj) {
        return {};
    }
    for (let key in obj) {
        let value = obj[key];
        value = JSON.stringify(value)
        invertedKeyValueObj[value] = key;
    }
    return invertedKeyValueObj;
}

module.exports = { invert };